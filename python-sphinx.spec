%global _empty_manifest_terminate_build 0
Name:		python-sphinx
Version:	5.3.0
Release:	1
Summary:	Python documentation generator
License:	BSD and MIT 
URL:		https://www.sphinx-doc.org/
Source0:	https://files.pythonhosted.org/packages/af/b2/02a43597980903483fe5eb081ee8e0ba2bb62ea43a70499484343795f3bf/Sphinx-5.3.0.tar.gz
Patch0:		0001-add-setup.py.patch
BuildArch:	noarch

Requires:	python-sphinx-locale = %{?epoch}:%{version}-%{release}
Requires:	python3-babel python3-docutils python3-jinja2 python3-pygments
Requires:	python3-snowballstemmer python3-sphinx_rtd_theme python3-sphinx-theme-alabaster
Requires:	python3-imagesize python3-requests python3-six python3-packaging
Requires:	environment(modules) python3-sphinxcontrib-websupport python3-mock
Requires(pre):	/usr/sbin/alternatives
Requires:	python(Sphinx) = %{?epoch}:%{version}-%{release}

%description
Sphinx is a tool that makes it easy to create intelligent and
beautiful documentation for Python projects (or other documents
consisting of multiple reStructuredText sources), written by Georg
Brandl. It was originally created to translate the new Python
documentation, but has now been cleaned up in the hope that it will be
useful to many other projects.

Sphinx uses reStructuredText as its markup language, and many of its
strengths come from the power and straightforwardness of
reStructuredText and its parsing and translating suite, the Docutils.

Although it is still under constant development, the following
features are already present, work fine and can be seen "in action" in
the Python docs:

    * Output formats: HTML (including Windows HTML Help) and LaTeX,
      for printable PDF versions
    * Extensive cross-references: semantic markup and automatic links
      for functions, classes, glossary terms and similar pieces of
      information
    * Hierarchical structure: easy definition of a document tree, with
      automatic links to siblings, parents and children
    * Automatic indices: general index as well as a module index
    * Code handling: automatic highlighting using the Pygments highlighter
    * Various extensions are available, e.g. for automatic testing of
      snippets and inclusion of appropriately formatted docstrings.

%package -n python3-sphinx
Summary:	Python documentation generator
Obsoletes:     python3-sphinxcontrib-napoleon < 0.3.0
Provides:	python-Sphinx = %{version}-%{release}
Provides:      python3-sphinxcontrib-napoleon = %{?epoch}:%{version}-%{release}
Provides:      python(Sphinx) = %{?epoch}:%{version}-%{release}
Conflicts:     python2-Sphinx < %{?epoch}:%{version}-%{release}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-babel
BuildRequires:	python3-docutils
BuildRequires:	python3-imagesize
BuildRequires:	python3-jinja2
BuildRequires:	python3-packaging
BuildRequires:	python3-pygments
BuildRequires:	python3-requests
BuildRequires:	python3-sphinxcontrib-applehelp
BuildRequires:	python3-sphinxcontrib-devhelp
BuildRequires:	python3-sphinxcontrib-htmlhelp
BuildRequires:	python3-sphinxcontrib-jsmath
BuildRequires:	python3-sphinxcontrib-qthelp
BuildRequires:	python3-sphinxcontrib-serializinghtml
BuildRequires:	python3-sphinx-theme-alabaster
BuildRequires:	dos2unix
BuildRequires:	python3-test
BuildRequires:	python3-html5lib
BuildRequires:	python3-mock
BuildRequires:	python3-pytest
BuildRequires:	python3-snowballstemmer
BuildRequires:	gettext
BuildRequires:	graphviz
BuildRequires:	texinfo

%description -n python3-sphinx
Sphinx is a tool that makes it easy to create intelligent and
beautiful documentation for Python projects (or other documents
consisting of multiple reStructuredText sources), written by Georg
Brandl. It was originally created to translate the new Python
documentation, but has now been cleaned up in the hope that it will be
useful to many other projects.

Sphinx uses reStructuredText as its markup language, and many of its
strengths come from the power and straightforwardness of
reStructuredText and its parsing and translating suite, the Docutils.

Although it is still under constant development, the following
features are already present, work fine and can be seen "in action" in
the Python docs:

    * Output formats: HTML (including Windows HTML Help) and LaTeX,
      for printable PDF versions
    * Extensive cross-references: semantic markup and automatic links
      for functions, classes, glossary terms and similar pieces of
      information
    * Hierarchical structure: easy definition of a document tree, with
      automatic links to siblings, parents and children
    * Automatic indices: general index as well as a module index
    * Code handling: automatic highlighting using the Pygments highlighter
    * Various extensions are available, e.g. for automatic testing of
      snippets and inclusion of appropriately formatted docstrings.

%package help
Summary:	Development documents and examples for Sphinx
Provides:      python-sphinx-doc python-sphinx-latex
Obsoletes:     python-sphinx-doc < %{?epoch}:%{version}-%{release}
Obsoletes:     python-sphinx-latex < %{?epoch}:%{version}-%{release}

%description help
This package contains help documentation in reST and HTML formats.

%package   locale
Summary:Locale files for python-sphinx

%description   locale
This package contains locale files for Sphinx.

%prep
%autosetup -n Sphinx-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-sphinx -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Mon Nov 14 2022 wangjunqi <wangjunqi@kylinos.cn> - 5.3.0-1
- Update package to version 5.3.0

* Thu Jun 16 2022 zhangy1317 <zhangy1317@foxmail.com> - 1:4.4.0-1
- Upgrade version for openstack yoga

* Mon Dec 13 2021 shixuantong <shixuantong@huawei.com> - 4.3.1-1
- update version to 4.3.1

* Tue Jul 27 2021 OpenStack_SIG <openstack@openeuler.org> - 3.5.2-1
- update to 3.5.2

* Wed Jan 13 2021 SimpleUpdate Robot <tc@openeuler.org> - 3.4.3-1
- Upgrade to version 3.4.3

* Wed Aug 5  2020 tianwei <tianwei12@huawei.com> - 3.1.2-2
- add package locale and help

* Fri Jul 31 2020 tianwei <tianwei12@huawei.com> - 3.1.2-1
- Package update to 3.1.2

* Thu Feb 20 2020 Lijin Yang <yanglijin@huawei.com> - 1:1.7.6-6
- delete useless files

* Thu Feb 20 2020 Lijin Yang <yanglijin@huawei.com> - 1:1.7.6-5
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: make sphinx-build enable

* Thu Nov 14 2019 Lijin Yang <yanglijin@huawei.com> - 1:1.7.6-4
- init package

